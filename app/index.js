'use strict';

const bytenode = require('bytenode');
const path = require('path');
const v8 = require('v8');
v8.setFlagsFromString('--no-lazy');

if(process.env.MAKE === "true"){
    bytenode.compileFile(path.join(__dirname + '/app.js'), path.join(__dirname + '/app.jsc'));
    bytenode.compileFile(path.join(__dirname + '/dist/index-bundle.js'), path.join(__dirname + '/dist/index-bundle.jsc'));

    require('fs').unlinkSync(path.join(__dirname + '/app.js'))
    require('fs').unlinkSync(path.join(__dirname + '/dist/index-bundle.js'))
    require('rimraf').sync(path.join(__dirname + "/javascript/"))
}else{
    try{
        bytenode.compileFile(path.join(__dirname + '/app.js'), path.join(__dirname + '/app.jsc'));
        bytenode.compileFile(path.join(__dirname + '/dist/index-bundle.js'), path.join(__dirname + '/dist/index-bundle.jsc'));
    }catch{}
}

require(path.join(__dirname + '/app.jsc'));