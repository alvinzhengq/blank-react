const crypto = require('crypto');

function encrypt(text, password, salt) {
    let iv = crypto.randomBytes(16).toString("hex");
    let key = crypto.scryptSync(password, Buffer.from(salt, 'hex'), 32);
    let cipher = crypto.createCipheriv('aes-256-cbc', key, Buffer.from(iv, 'hex'));

    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);

    return {data: encrypted.toString('hex'), iv: iv};
}

function decrypt(encryptedObj, password, salt, iv) {
    let key = crypto.scryptSync(password, Buffer.from(salt, 'hex'), 32);

    let encryptedText = Buffer.from(encryptedObj, 'hex');
    let decipher = crypto.createDecipheriv('aes-256-cbc', key, Buffer.from(iv, 'hex'));

    try{
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);

        return decrypted.toString();
    }catch(error){
        return "WRONG"
    }
}

export {
    encrypt,
    decrypt
}