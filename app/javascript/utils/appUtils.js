import * as crypto from 'crypto';
import { sha512 } from 'js-sha512';
import * as fs from 'fs';
import * as fileEncryption from './fileEncryption';
import NodeRSA from 'node-rsa';
import * as $ from 'jquery';
import {cookiesFile, encryptionFile, dataDir } from '../configs';

function validateRegisterFormInput(email, password, password_confirm, username){
    if(password != "" && password_confirm != "" && username != "" && email != ""){
        if(!/\s/.test(username) && !/\s/.test(password) && /^[0-9a-zA-Z]*$/.test(username)){
            if(/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{10,}$/.test(password)){
                if(password === password_confirm){
                    if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
                        return "VALID";
                    }else{
                        return "Invalid Email";
                    }
                }else{
                    return "Passwords do not match";
                }
            }else{
                return "Password must contain at least 1 upper case letter, 1 lowercase letter, 1 digit, 1 special character, and minimum 10 in length"
            }
        }else{
            return "Username or password contains invalid characters";
        }
    }else{
        return "One or more forms are empty";
    }
}

function testPasswordStrength(password){
    if(/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{10,}$/.test(password)){
        return true;
    }else{
        return false;
    }
}

function generateSalt() {
    return crypto.randomBytes(64).toString("hex");
}

function keyStretch(password){
    for(let i = 0; i < 1000; i++){
        password = sha512(password);
    }

    return password;
}

function setCookie(data, name, passcode){
    var encryptionData = JSON.parse(fs.readFileSync(encryptionFile, 'utf-8'))
    var cookiesDataFile = JSON.parse(fs.readFileSync(cookiesFile, 'utf-8'));
    var cookies = JSON.parse(
        fileEncryption.decrypt(
            cookiesDataFile.data, passcode, encryptionData.salt, cookiesDataFile.iv
        )
    )

    cookies[name] = data;
    var data = fileEncryption.encrypt(JSON.stringify(cookies), passcode, encryptionData.salt);
    fs.writeFileSync(cookiesFile, JSON.stringify(data));
}

function shouldGenNewKey(username, passcode){
    console.log("test")
    if(!fs.existsSync(dataDir + `/${username}/keyPair`)){
        return true;
    }else{
        var encryptionData = JSON.parse(fs.readFileSync(encryptionFile, 'utf-8'))
        var keyPairDataFile = JSON.parse(fs.readFileSync(dataDir + `/${username}/keyPair`, 'utf-8'));
        var keyPair = JSON.parse(
            fileEncryption.decrypt(
                keyPairDataFile.data, passcode, encryptionData.salt, keyPairDataFile.iv
            )
        )

        var currentDate = new Date();
        currentDate.setHours(0, 0, 0 ,0);

        if(keyPair.expireTime <= currentDate.getTime()){
            return true;
        }else{
            return false;
        }
    }
}

function genKeys(username, passcode, cb){
    var encryptionData = JSON.parse(fs.readFileSync(encryptionFile, 'utf-8'))
    var keyPair = new NodeRSA({b: 2048}).generateKeyPair();
    var pubKey = keyPair.exportKey('pkcs1-public-pem');
    var privKey = keyPair.exportKey('pkcs1-private-pem');

    var expireTime = new Date();
    expireTime.setHours(720, 0, 0, 0);

    var keyPairObj = {};
    keyPairObj.pubKey = pubKey;
    keyPairObj.privKey = privKey;
    keyPairObj.expireTime = expireTime;

    var encryptedKeyPairObj = fileEncryption.encrypt(JSON.stringify(keyPairObj), passcode, encryptionData.salt)

    // var oldKeyPairData = JSON.parse(fs.readFileSync(dataDir + `/${username}/keyPair`));
    // var oldKeyPairObj = JSON.parse(fileEncryption.decrypt(oldKeyPairData.data, passcode, encryptionData.salt, oldKeyPairData.iv));

    // fs.copyFileSync(dataDir + `/${username}/keyPair`, dataDir + `/${username}/keyPair_old`);
    fs.writeFileSync(dataDir + `/${username}/keyPair`, JSON.stringify(encryptedKeyPairObj), { flag: 'w+' });

    cb({newPubKey: pubKey, newPrivKey: keyPairObj.privKey});
}

function getPrivKey(username, passcode, cb){
    var encryptionData = JSON.parse(fs.readFileSync(encryptionFile, 'utf-8'))
    var KeyPairData = JSON.parse(fs.readFileSync(dataDir + `/${username}/keyPair`));
    var KeyPairObj = JSON.parse(fileEncryption.decrypt(KeyPairData.data, passcode, encryptionData.salt, KeyPairData.iv));

    cb(KeyPairObj.privKey);
}

function createDataFiles(password){
    var salt = crypto.randomBytes(16).toString("hex");

    fs.writeFileSync(encryptionFile, JSON.stringify({
        salt: salt.toString("hex")
    }))

    fs.writeFileSync(cookiesFile, JSON.stringify(fileEncryption.encrypt(JSON.stringify({}), password, salt)));
}

function lockCheck(password){
    var testData = JSON.parse(fs.readFileSync(cookiesFile, "utf-8"));
    var encryptionData = JSON.parse(fs.readFileSync(encryptionFile, 'utf-8'))

    if(fileEncryption.decrypt(testData.data, password, encryptionData.salt, testData.iv) != "WRONG"){
        return true;
    }else{
        return false;
    }
}

function loginCheck(passcode, cb) {
    var encryptionData = JSON.parse(fs.readFileSync(encryptionFile, 'utf-8'))
    var cookiesDataFile = JSON.parse(fs.readFileSync(cookiesFile, 'utf-8'));
    var cookies = JSON.parse(
        fileEncryption.decrypt(
            cookiesDataFile.data, passcode, encryptionData.salt, cookiesDataFile.iv
        )
    )

    if($.isEmptyObject(cookies)){
        cb("REGISTER")
    }else{
        cb(cookies);
    }
}

export {
    validateRegisterFormInput,
    generateSalt,
    keyStretch,
    setCookie,
    createDataFiles,
    testPasswordStrength,
    lockCheck,
    loginCheck,
    genKeys,
    shouldGenNewKey,
    getPrivKey
}