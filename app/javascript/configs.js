import Electron from 'electron';
import * as path from 'path';
import * as seedrandom from 'seedrandom';
import NodeRSA from 'node-rsa'

let tempSec = ["nV!Q6hCc5ur$#uMh", "^e2Z+DXt8Qqmbjm8", "B!?ACVkJ3uD&bW+u", "Tjp%6&7n6H44?c^^", "J$_E%SJPgg3dAqqu", "dZ%TJx*9#shKuJ3-", "P6_F5aaZVK!hnq5_", "XXhcX$_s*M45K3Lu", "?nuawX+QF3G++3d^"]
let apiSecret = ""
for(let i = 0; i < tempSec.length; i++){
    apiSecret += seedrandom(tempSec[i])().toString(36).substring(2, 15);
}

let dataDir;
if(process.platform === "linux"){
    dataDir = path.resolve(require('os').homedir(), '.Blank')
}else if(process.platform === "win32"){
    dataDir = path.resolve(process.env.APPDATA, '.Blank')
}

export {dataDir}
export const encryptionFile = path.join(dataDir + "/encryptionData");
export const cookiesFile = path.join(dataDir + "/cookies");
export const messagesFolder = path.join(dataDir + "/messages/");
export const backUpFolder = path.join(dataDir + "/_backUps")
export const rootDir = Electron.remote.app.getAppPath();
export const serverURL = "http://localhost:3000/";
export {apiSecret}
export const apiPubKey = new NodeRSA().importKey("-----BEGIN RSA PUBLIC KEY-----MIIBCgKCAQEAj+B2k5FMhahb+nobp5t3xY+Ly9vFqdGiNaqing7OetRU/ejMOwPOnRZv7mjt9krT2bNY0XDRalOeakRbqiVlszq3/N67v6hC0weXGoWwZptgnZHrY3CLlsUtAYBaTg8dQHg5g6JoT/OUESIyEqLgdXKdHSA/9qTl6ZtwHXjKX9P1rb22dTYNVaxBokAT+Qvmo0OS2jvbGPNW3MdAWZQS4fdepZtWCHwSd/hlAZj+OkHi2aDdoLjluTpSJvC1e/aeD8CYMjjT/7fDraqAVzQU+ox3cLfmwA7xIWiRqzHo4Ioocbo56ljS4Dr4l2eZCij6TYoD/eU2pViR4YiZZzB9DQIDAQAB-----END RSA PUBLIC KEY-----", "pkcs1-public-pem")
export const axiosHeaders = { 'Content-Type': 'text/plain' }