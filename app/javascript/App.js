import React from 'react';
import * as fs from 'fs';
import * as configs from './configs';

import LockForm from './pages/forms/LockForm';
import CreateLock from './pages/forms/CreateLock';
import Register from './pages/forms/Register';
import VerifyAcc from './pages/forms/VerifyAcc';
import LRVNav from './pages/forms/LRVNav';
import Login from './pages/forms/Login';
import ResetPassword from './pages/forms/ResetPassword';
import MainApp from './pages/mainApp/MainApp';

import '../stylesheets/bootstrap.css';
import '../stylesheets/master.css';
import '../stylesheets/forms.css'
import '../stylesheets/appSettings.css';
import '../stylesheets/app.css';

class App extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            currentView: "",
            encryptionPasscode: "",
            refreshToken: "",
            username: ""
        }

        if(!fs.existsSync(configs.dataDir)){
            fs.mkdirSync(configs.dataDir);
            fs.mkdirSync(configs.messagesFolder);
            fs.mkdirSync(configs.backUpFolder);
            fs.writeFileSync(configs.encryptionFile, "")
            fs.writeFileSync(configs.cookiesFile, "");
        }

        if(fs.readFileSync(configs.encryptionFile) == ''){
            this.state.currentView = "createLock";
        }else{
            this.state.currentView = "lock";
        }

        this.changeView = this.changeView.bind(this);
        this.setEncryptionPasscode = this.setEncryptionPasscode.bind(this)
        this.setRefreshToken = this.setRefreshToken.bind(this)
        this.setUsername = this.setUsername.bind(this)
    }

    changeView(new_view){
        this.setState({
            currentView: new_view
        })
    }

    setEncryptionPasscode(passcode){
        this.setState({
            encryptionPasscode: passcode
        })
    }

    setRefreshToken(token){
        this.setState({
            refreshToken: token
        })
    }

    setUsername(username){
        this.setState({
            username: username
        })
    }

    render() {
        const currentView = this.state.currentView;
        let view;

        if(currentView === "lock"){
            view = <LockForm changeView={this.changeView} setPasscode={this.setEncryptionPasscode} setUsername={this.setUsername} setRefreshToken={this.setRefreshToken}/>;

        }else if(currentView === "lrvNav"){
            view = <LRVNav changeView={this.changeView}/>

        }else if(currentView === "createLock"){
            view = <CreateLock changeView={this.changeView}/>;

        }else if(currentView === "register"){
            view = <Register changeView={this.changeView}/>

        }else if(currentView === "verify"){
            view = <VerifyAcc changeView={this.changeView}/>

        }else if(currentView === "login"){
            view = <Login changeView={this.changeView} setRefreshToken={this.setRefreshToken} setUsername={this.setUsername} passcode={this.state.encryptionPasscode}/>

        }else if(currentView === "passwordReset"){
            view = <ResetPassword changeView={this.changeView}/>

        }else if(currentView === "app"){
            view = <MainApp changeView={this.changeView} username={this.state.username} refreshToken={this.state.refreshToken} passcode={this.state.encryptionPasscode}/>
        }

        return(
            <>
                { view }
            </>
        )
    }
}

export default App