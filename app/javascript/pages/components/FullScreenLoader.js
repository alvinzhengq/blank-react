import React from 'react';
import '../../../stylesheets/components/fullScreenLoader.css'

import { css } from "@emotion/core";
import ClipLoader from "react-spinners/ClipLoader";

class FullScreenLoader extends React.Component {
    constructor(props){
        super(props)
    }

    render() {
        return(
            <div className={this.props.visible ? "fullScreenLoaderContainerFadeIn" : "fullScreenLoaderContainerFadeOut"}>
                <div className="FullScreenLoader">
                    <ClipLoader size={80} color={'#000000'} loading={true}/>
                    <h1 className="fullScreenLoaderText">{this.props.text != undefined ? this.props.text : ""}</h1>
                </div>
            </div>
        )
    }
}

export default FullScreenLoader