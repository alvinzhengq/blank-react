import React from 'react';
import io from '../../libs/socket.io';
import axios from 'axios';
import {serverURL, apiPubKey, axiosHeaders, apiSecret} from '../../configs';
import {genKeys, shouldGenNewKey, getPrivKey} from '../../utils/appUtils';
import { machineIdSync } from 'node-machine-id';

import FullScreenLoader from '../components/FullScreenLoader';
import AppView from './AppView';
import AppSettings from './AppSettings';

class MainApp extends React.PureComponent {
    constructor(props){
        super(props)

        this.state = {
            currentView: "establishConnection",
            loadingStatus: "",
            authToken: "",
            socket: null,
            chatDataList: null,
            oldPrivKey: null,
            privKey: null
        }

        this.changeAppView = this.changeAppView.bind(this);
    }

    componentDidMount(){
        this._isMounted = true;

        const establishSocket = () => {
            var tempSocket = io(serverURL, {reconnection: false});
            tempSocket.emit('authenticate', {token: this.state.authToken})
            tempSocket.on('authenticated', () => {
                console.log("test")
                this.setState({
                    socket: tempSocket,
                    loadingStatus: "Doing magic stuff behind the scenes"
                })


                if(shouldGenNewKey(this.props.username, this.props.passcode)){
                    genKeys(this.props.username, this.props.passcode, (dataObj)=>{
                        tempSocket.emit('updateKey', dataObj.pubKey);
                        tempSocket.emit('getChatDataList', '');
                        this.setState({
                            privKey: dataObj.newPrivKey
                        })
                    });
                }else{
                    getPrivKey(this.props.username, this.props.passcode, (privKey) => {
                        tempSocket.emit('getChatDataList', '');
                        this.setState({
                            privKey: privKey
                        })
                    })
                }

                tempSocket.on('chatDataList', (chatDataList)=>{
                    this.setState({
                        chatDataList: chatDataList,
                        currentView: "app"
                    })
                    tempSocket.emit('getOfflineData', '');
                })

                tempSocket.on('offlineData', (offlineDataList) => {
                    console.log(offlineDataList)
                })
            })
            tempSocket.on('unauthorized', (msg) => {
                this.setState({
                    loadingStatus: "Unauthorized, redirecting to loading screen..."
                })

                setTimeout(()=>{
                    this.props.changeView("login");
                }, 3000)
            })
            tempSocket.on('disconnect', (reason) => {
                getAuthToken();
            })

            let disconnectEvent;
            tempSocket.on('ping', () => {
                disconnectEvent = setTimeout(()=>{
                    getAuthToken();
                }, 20000)
            })
            tempSocket.on('pong', () => {
                clearTimeout(disconnectEvent);
            })
        }
        const getAuthToken = () => {
            if(!this._isMounted){
                return;
            }

            this.setState({
                currentView: "establishConnection",
                loadingStatus: "Connecting to server"
            })

            axios.post(serverURL + "api/getToken", apiPubKey.encrypt(JSON.stringify({
                username: this.props.username,
                refreshToken: this.props.refreshToken,
                machineID: machineIdSync(),
                apiSecret: apiSecret
            }), "hex"), {headers: axiosHeaders})
            .then((res) => {
                if(res.data === "INVALID"){
                    this.props.changeView("login")
                }else if(res.data === "SECONDARY"){
                    this.setState({
                        currentView: "secondary_connect"
                    })
                }else{
                    this.setState({
                        authToken: res.data,
                        loadingStatus: "Establishing socket connection"
                    })

                    establishSocket();
                }
            })
            .catch(err => {
                setTimeout(getAuthToken, 1000);
            })
        }

        getAuthToken();
    }

    componentWillUnmount(){
        this._isMounted = false;
        try {
            this.state.socket.close();
        } catch (error) {}
    }

    changeAppView(new_view){
        this.setState({
            currentView: new_view
        })
    }

    render() {
        const currentView = this.state.currentView;
        let view;

        if(currentView === "establishConnection"){
            view = <FullScreenLoader visible={this.state.currentView === "establishConnection"} text={this.state.loadingStatus}></FullScreenLoader>
        }else if(currentView === "app"){
            view = <AppView changeAppView={this.changeAppView} WebSocket={this.state.socket}/>
        }else if(currentView === "settings"){
            view = <AppSettings changeMasterView={this.props.changeView} changeAppView={this.changeAppView} refreshToken={this.props.refreshToken} username={this.props.username}/>
        }else if(currentView === "secondary_connect"){
            view = <div>Secondary Device</div>
        }

        return(
            <>
                { view }
            </>
        )
    }
}

export default MainApp