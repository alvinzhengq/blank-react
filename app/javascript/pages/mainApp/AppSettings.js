import React from 'react';
import axios from 'axios';
import { machineIdSync } from 'node-machine-id';
import { serverURL, apiSecret, apiPubKey, axiosHeaders, dataDir } from '../../configs';
import { testPasswordStrength, generateSalt, keyStretch } from '../../utils/appUtils';
import fs from 'fs';
import fsExtra from 'fs-extra';

import BackButton from '../components/BackButton';
import FullScreenLoader from '../components/FullScreenLoader';

import { CardBody, Card, CardTitle, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Button, InputGroup, InputGroupAddon, InputGroupText, Input } from 'reactstrap';

class AppSettings extends React.PureComponent {
    constructor(props){
        super(props);

        this.state = {
            buttonClicked: false,
            loadingStatus: "",
            password: "",
            confirmPass: "",
            deleteAccModal: false,
            confirmDeleteText: "",
            passwordStatus: false,
            emailStatus: false,
            email: ""
        }

        this.logout = this.logout.bind(this)
        this.updateInputs = this.updateInputs.bind(this)
        this.deleteAcc = this.deleteAcc.bind(this),
        this.changePassword = this.changePassword.bind(this)
        this.handleStatus = this.handleStatus.bind(this)
        this.changeEmail = this.changeEmail.bind(this)
    }

    updateInputs(evt){
        this.setState({
            [evt.target.name]: evt.target.value
        })
    }

    deleteAcc(evt){
        if(evt.target.value === "deleteAcc"){
            this.setState({
                deleteAccModal: true
            })
        }else{
            this.setState({
                buttonClicked: true,
                loadingStatus: "Deleting Account",
                deleteAccModal: false
            })

            axios.post(serverURL + 'api/getToken', apiPubKey.encrypt(JSON.stringify({
                username: this.props.username,
                refreshToken: this.props.refreshToken,
                machineID: machineIdSync(),
                apiSecret: apiSecret
            }), "hex"), {headers: axiosHeaders})
            .then((res) => {
                if(res.data != "INVALID" || res.data != "SECONDARY"){
                    axios.post(serverURL + 'api/deleteAccount', apiPubKey.encrypt(JSON.stringify({
                        authToken: res.data,
                        apiSecret: apiSecret
                    }), 'hex'), {headers: axiosHeaders})
                    .then((res2) => {
                        if(res2.data == "GOOD"){
                            fsExtra.copySync(dataDir + "/" + this.props.username, dataDir + "/_backUp/" + this.props.username, {overwrite: true})
                            fs.rmdirSync(dataDir + "/" + this.props.username, { recursive: true });
                            this.props.changeMasterView("login");
                        }else{
                            this.setState({
                                loadingStatus: "Invalid, redirecting to login screen"
                            })
                            this.props.changeMasterView("login");
                        }
                    })
                }else{
                    this.setState({
                        loadingStatus: "Invalid, redirecting to login screen"
                    })
                    this.props.changeMasterView("login");
                }
            })
        }
    }

    logout() {
        this.setState({
            buttonClicked: true,
            loadingStatus: "Logging out"
        })

        axios.post(serverURL + 'api/getToken', apiPubKey.encrypt(JSON.stringify({
            username: this.props.username,
            refreshToken: this.props.refreshToken,
            machineID: machineIdSync(),
            apiSecret: apiSecret
        }), "hex"), {headers: axiosHeaders})
        .then((res) => {
            if(res.data != "INVALID" || res.data != "SECONDARY"){
                axios.post(serverURL + 'api/logout', apiPubKey.encrypt(JSON.stringify({
                    authToken: res.data,
                    apiSecret: apiSecret
                }), 'hex'), {headers: axiosHeaders})
                .then((res2) => {
                    this.setState({
                        loadingStatus: "Success, redirecting to login screen"
                    })
                    this.props.changeMasterView("login");
                })
                .catch((err) => {
                    this.setState({
                        buttonClicked: false
                    })
                })
            }else{
                this.setState({
                    loadingStatus: "Invalid, redirecting to login screen"
                })
                this.props.changeMasterView("login");
            }
        })
        .catch((err) => {
            this.setState({
                buttonClicked: false
            })
        })
    }

    changePassword(){
        this.setState({
            buttonClicked: true,
            loadingStatus: "Changing Password"
        })

        if(this.state.password == this.state.confirmPass){
            if(testPasswordStrength(this.state.password)){
                let salt = generateSalt();
                let finalPassword = keyStretch(this.state.password + salt);

                axios.post(serverURL + 'api/getToken', apiPubKey.encrypt(JSON.stringify({
                    username: this.props.username,
                    refreshToken: this.props.refreshToken,
                    machineID: machineIdSync(),
                    apiSecret: apiSecret
                }), "hex"), {headers: axiosHeaders})
                .then((res) => {
                    if(res.data != "INVALID" || res.data != "SECONDARY"){
                        axios.post(serverURL + 'api/changePassword', apiPubKey.encrypt(JSON.stringify({
                            authToken: res.data,
                            salt: salt,
                            password: finalPassword,
                            apiSecret: apiSecret
                        }), 'hex'), {headers: axiosHeaders})
                        .then((res2) => {
                            if(res2.data == "GOOD"){
                                this.setState({
                                    loadingStatus: "Success"
                                })
                                setTimeout(()=>{
                                    this.setState({
                                        buttonClicked: false,
                                        password: "",
                                        confirmPass: ""
                                    })
                                }, 2000)
                            }else{
                                this.setState({
                                    loadingStatus: "Invalid, redirecting to login screen"
                                })
                                this.props.changeMasterView("login");
                            }
                        })
                        .catch((err) => {
                            this.setState({
                                buttonClicked: false
                            })
                        })
                    }else{
                        this.setState({
                            loadingStatus: "Invalid, redirecting to login screen"
                        })
                        this.props.changeMasterView("login");
                    }
                })
                .catch(err => {
                    this.setState({
                        buttonClicked: false
                    })
                })

            }else{
                this.handleStatus("Password must contain at least 1 upper case letter, 1 lowercase letter, 1 digit, 1 special character, and minimum 10 in length", "passwordStatus")
                this.setState({
                    buttonClicked: false
                })
            }
        }else{
            this.handleStatus("Passwords do not match", "passwordStatus")
            this.setState({
                buttonClicked: false
            })
        }
    }

    changeEmail(){
        this.setState({
            buttonClicked: true,
            loadingStatus: "Changing Email"
        })

        if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email)){
            axios.post(serverURL + 'api/getToken', apiPubKey.encrypt(JSON.stringify({
                username: this.props.username,
                refreshToken: this.props.refreshToken,
                machineID: machineIdSync(),
                apiSecret: apiSecret
            }), "hex"), {headers: axiosHeaders})
            .then((res) => {
                if(res.data != "INVALID" || res.data != "SECONDARY"){
                    axios.post(serverURL + 'api/changeEmail', apiPubKey.encrypt(JSON.stringify({
                        authToken: res.data,
                        email: this.state.email,
                        apiSecret: apiSecret
                    }), 'hex'), {headers: axiosHeaders})
                    .then((res2) => {
                        if(res2.data == "GOOD"){
                            this.setState({
                                loadingStatus: "Success"
                            })
                            setTimeout(()=>{
                                this.setState({
                                    buttonClicked: false,
                                    email: ""
                                })
                            }, 2000)
                        }else{
                            this.setState({
                                loadingStatus: "Invalid, redirecting to login screen"
                            })
                            this.props.changeMasterView("login");
                        }
                    })
                    .catch((err) => {
                        this.setState({
                            buttonClicked: false
                        })
                    })
                }else{
                    this.setState({
                        loadingStatus: "Invalid, redirecting to login screen"
                    })
                    this.props.changeMasterView("login");
                }
            })

        }else{
            this.handleStatus("Invalid email", "emailStatus")
        }

    }

    handleStatus(message, status){
        document.getElementsByClassName(status)[0].innerHTML = message;

        this.setState({
            [status]: true
        })
        setTimeout(()=>{
            this.setState({
                [status]: false
            })
        }, 5000)
    }

    render() {
        return(
            <div className="settingsContainer">
                <h1 className="title">App Settings</h1>
                <hr/>

                <button onClick={this.logout} type="submit">Logout</button>

                <Col sm="12" md={{ size: 6, offset: 3 }}>
                    <Row>
                        <Card body inverse style={{background: 'rgba(0, 0, 0, 0)', borderWidth: '2px', marginTop: '3vh', width: '70vw'}}>
                            <CardTitle>
                                <h2>Change Password</h2>
                            </CardTitle>
                            <CardBody>
                                <input type="password" placeholder="Password" name="password" onChange={this.updateInputs} required/>
                                <br/>
                                <input type="password" placeholder="Confirm Password" name="confirmPass" onChange={this.updateInputs} required/>
                                <br/>
                                <button onClick={this.changePassword} type="submit" disabled={this.state.password == "" || this.state.confirmPass == ""}>Change Password</button>
                                <p className={this.state.passwordStatus ? 'fadeInStatus passwordStatus status' : 'fadeOutStatus passwordStatus status'}>DUMMY TEXT</p>
                            </CardBody>
                        </Card>
                    </Row>

                    <Row>
                        <Card body inverse style={{background: 'rgba(0, 0, 0, 0)', borderWidth: '2px', marginTop: '3vh', width: '70vw'}}>
                            <CardTitle>
                                <h2>Change Email</h2>
                            </CardTitle>
                            <CardBody>
                                <input type="text" placeholder="New Email" name="email" onChange={this.updateInputs} required/>
                                <br/>
                                <button onClick={this.changeEmail} type="submit" disabled={this.state.email == ""}>Change Email</button>
                                <p className={this.state.emailStatus ? 'fadeInStatus emailStatus status' : 'fadeOutStatus emailStatus status'}>DUMMY TEXT</p>
                            </CardBody>
                        </Card>
                    </Row>
                </Col>

                <Button color="danger" onClick={this.deleteAcc} style={{marginTop: '3vh'}} value="deleteAcc">Delete Account</Button>

                <Modal isOpen={this.state.deleteAccModal} backdrop="static" keyboard={false}>
                    <ModalHeader>Confirm Delete Account</ModalHeader>
                        <ModalBody style={{textAlign: 'center'}}>
                            <h3>Type in <b>{this.props.username}</b> to confirm the deletion of this account</h3>
                            <InputGroup>
                                <Input placeholder="Confirm Delete" name="confirmDeleteText" onChange={this.updateInputs}/>
                            </InputGroup>
                        </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.deleteAcc} value="confirmDel" disabled={this.props.username != this.state.confirmDeleteText}>Confirm Delete</Button>{' '}
                        <Button color="secondary" onClick={() => { this.setState({deleteAccModal: false}) }}>Cancel</Button>
                    </ModalFooter>
                </Modal>

                <BackButton changeView={() => this.props.changeAppView("app")}/>
                <FullScreenLoader visible={this.state.buttonClicked} text={this.state.loadingStatus}/>
            </div>
        )
    }
}

export default AppSettings