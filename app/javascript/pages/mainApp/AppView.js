import React from 'react';

class AppViews extends React.PureComponent {
    constructor(props){
        super(props);
    }

    render() {
        return(
            <>
                <button onClick={() => this.props.changeAppView("settings")} type="submit">Settings</button>
            </>
        )
    }
}

export default AppViews