import React from 'react';
import axios from 'axios'
import { serverURL, apiPubKey, apiSecret, axiosHeaders } from '../../configs';
import {setCookie} from '../../utils/appUtils';

import FullScreenLoader from '../components/FullScreenLoader';
import BackButton from '../components/BackButton';

class Login extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            username: "",
            password: "",
            buttonClicked: "",
            statusVisible: ""
        }

        this.updateInputs = this.updateInputs.bind(this)
        this.handleStatus = this.handleStatus.bind(this)
        this.handleClick = this.handleClick.bind(this)
    }

    handleStatus(message){
        document.getElementsByClassName("status")[0].innerHTML = message;

        this.setState({
            statusVisible: true
        })
        setTimeout(()=>{
            this.setState({
                statusVisible: false
            })
        }, 5000)
    }

    updateInputs(evt) {
        this.setState({
            [evt.target.name]: evt.target.value
        })
    }

    handleClick(){
        this.setState({
            buttonClicked: true
        })

        if(this.state.username != "" && this.state.password != ""){
            axios.post(serverURL+"api/login", apiPubKey.encrypt(JSON.stringify({
                username: this.state.username,
                password: this.state.password,
                apiSecret: apiSecret
            }), 'hex'), {headers: axiosHeaders})
            .then((res) => {
                if(res.data.status === "GOOD"){
                    this.setState({
                        buttonClicked: false
                    })
                    setCookie(res.data.jwt, "refreshToken", this.props.passcode);
                    setCookie(this.state.username, "username", this.props.passcode)
                    this.props.setUsername(this.state.username)
                    this.props.setRefreshToken(res.data.jwt);
                    this.props.changeView("app");

                }else if(res.data.status === "INVALID"){
                    this.setState({
                        buttonClicked: false
                    })
                    this.handleStatus("Username or password is invalid")

                }else if(res.data.status === "EXPIRE"){
                    this.setState({
                        buttonClicked: false
                    })
                    this.handleStatus("Transport session expired, please try again");

                }else if(res.data.status === "ERROR"){
                    this.setState({
                        buttonClicked: false
                    })
                    this.handleStatus("Server side error");

                }
            })
            .catch((err) => {
                this.setState({
                    buttonClicked: false
                })
                this.handleStatus(err2);
            })
        }else{
            this.setState({
                buttonClicked: false
            })
            this.handleStatus("One or more forms are empty")
        }
    }

    render() {
        return(
            <div className="container">
                <h1 className="formTitle">Please Login</h1>
                <hr/>
                <input type="text" placeholder="Username" name="username" onChange={this.updateInputs} required/>
                <br/>
                <input type="password" placeholder="Password" name="password" onChange={this.updateInputs} required/>
                <br/>
                <button onClick={this.handleClick} disabled={this.state.buttonClicked} type="submit">Submit</button>

                <p className={this.state.statusVisible ? 'fadeInStatus status' : 'fadeOutStatus status'}></p>

                <FullScreenLoader visible={this.state.buttonClicked}/>
                <BackButton changeView={() => this.props.changeView("lrvNav")}/>
            </div>
        )
    }

}

export default Login