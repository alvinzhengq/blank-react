import React, { Component } from 'react'
import axios from 'axios';
import {serverURL, apiPubKey, axiosHeaders, apiSecret} from '../../configs';
import {testPasswordStrength, keyStretch, generateSalt} from '../../utils/appUtils';
import { Collapse, CardBody, Card } from 'reactstrap';

import BackButton from '../components/BackButton';
import FullScreenLoader from '../components/FullScreenLoader';

export default class ResetPassword extends Component {
    constructor(props){
        super(props);

        this.state = {
            buttonClicked: false,
            email: "",
            code: "",
            password: "",
            confirm: "",
            statusVisible: false,
            loadingStatus: "",
            codeVisible: false,
            resetVisible: false
        }

        this.updateInputs = this.updateInputs.bind(this)
        this.getCode = this.getCode.bind(this)
        this.handleStatus = this.handleStatus.bind(this)
        this.resetPassword = this.resetPassword.bind(this)
        this.toggleVisible = this.toggleVisible.bind(this)
    }

    updateInputs(evt){
        this.setState({
            [evt.target.name]: evt.target.value
        })
    }

    toggleVisible(evt){
        this.setState({
            [evt.target.name]: !this.state[evt.target.name]
        })
    }

    handleStatus(message){
        document.getElementsByClassName("status")[0].innerHTML = message;

        this.setState({
            statusVisible: true
        })
        setTimeout(()=>{
            this.setState({
                statusVisible: false
            })
        }, 5000)
    }

    getCode(){
        this.setState({
            buttonClicked: true,
            loadingStatus: "Sending code..."
        })
        if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email)){
            let resetCodeExpire = parseInt(localStorage.getItem("resetCodeExpire" + this.state.email))
            if(resetCodeExpire != NaN && Date.now() < resetCodeExpire){
                this.handleStatus("A reset code already exists, please check your email")
                this.setState({
                    buttonClicked: false,
                })
                return;
            }

            axios.post(serverURL + "api/getResetCode", apiPubKey.encrypt(JSON.stringify({
                email: this.state.email,
                apiSecret: apiSecret
            }), 'hex'), {headers: axiosHeaders})
            .then((res)=>{
                if(res.data === "GOOD"){
                    this.setState({
                        buttonClicked: false,
                    })

                    localStorage.setItem("resetCodeExpire" + this.state.email, Date.now()+125000)
                }else if(res.data == "ERROR"){
                    this.handleStatus("Server side error")
                    this.setState({
                        buttonClicked: false
                    })
                }else if(res.data == "NO_EXIST"){
                    this.handleStatus("This email is not associated with any accounts")
                    this.setState({
                        buttonClicked: false
                    })
                }
            })
        }else{
            this.handleStatus("Invalid email")
            this.setState({
                buttonClicked: false
            })
        }
    }

    resetPassword() {
        this.setState({
            buttonClicked: true,
            loadingStatus: "Resetting password..."
        })

        if(this.state.password === this.state.confirm){
            if(testPasswordStrength(this.state.password)){
                let salt = generateSalt();
                let finalPassword = keyStretch(this.state.password + salt);

                axios.post(serverURL + "api/resetPassword", apiPubKey.encrypt(JSON.stringify({
                    code: this.state.code,
                    password: finalPassword,
                    salt: salt,
                    apiSecret: apiSecret
                }), 'hex'), {headers: axiosHeaders})
                .then((res)=>{
                    if(res.data === "GOOD"){
                        this.handleStatus("Password changed successfully")
                        this.setState({
                            buttonClicked: false
                        })
                    }else if(res.data === "INVALID"){
                        this.handleStatus("Code invalid")
                        this.setState({
                            buttonClicked: false
                        })
                    }else{
                        this.handleStatus("Server side error")
                        this.setState({
                            buttonClicked: false
                        })
                    }
                })
                .catch((err)=>{
                    this.handleStatus(err)
                    this.setState({
                        buttonClicked: false
                    })
                })

            }else{
                this.handleStatus("Password must contain at least 1 upper case letter, 1 lowercase letter, 1 digit, 1 special character, and minimum 10 in length")
                this.setState({
                    buttonClicked: false
                })
            }
        }else{
            this.handleStatus("Passwords are not equal")
            this.setState({
                buttonClicked: false
            })
        }
    }

    render() {
        return (
            <>
                <div className="container">
                    <button onClick={this.toggleVisible} type="submit" name="codeVisible" style={{marginBottom: '2vh'}}>Get Reset Code</button>
                    <Collapse isOpen={this.state.codeVisible}>
                        <Card body inverse style={{background: 'rgba(0, 0, 0, 0)', borderWidth: '2px'}}>
                            <CardBody>
                                <h1 className="formTitle">Get Reset Code</h1>
                                <input type="text" placeholder="Email" name="email" onChange={this.updateInputs} required/>
                                <br/>
                                <button onClick={this.getCode} disabled={this.state.buttonClicked} type="submit">Get Reset Code</button>
                                <br/>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>

                <hr/>

                <div className="container" style={{marginTop: '-5vh'}}>
                    <button onClick={this.toggleVisible} type="submit" name="resetVisible" style={{marginBottom: '2vh'}}>Reset Password</button>
                    <Collapse isOpen={this.state.resetVisible}>
                        <Card body inverse style={{background: 'rgba(0, 0, 0, 0)', borderWidth: '2px'}}>
                            <CardBody>
                                <h1 className="formTitle">Reset Password</h1>
                                <input type="text" placeholder="Reset Code" name="code" onChange={this.updateInputs} required/>
                                <br/>
                                <input type="password" placeholder="Password" name="password" onChange={this.updateInputs} required/>
                                <br/>
                                <input type="password" placeholder="Confirm Password" name="confirm" onChange={this.updateInputs} required/>
                                <br/>
                                <button onClick={this.resetPassword} disabled={this.state.buttonClicked} type="submit">Reset</button>
                                <p className={this.state.statusVisible ? 'fadeInStatus status' : 'fadeOutStatus status'}></p>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>

                <FullScreenLoader visible={this.state.buttonClicked} text={this.state.loadingStatus}/>
                <BackButton changeView={() => this.props.changeView("lrvNav")}/>
            </>
        )
    }
}
