import React from 'react';

class LRVNav extends React.Component {
    constructor(props){
        super(props)

        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(evt){
        this.props.changeView(evt.target.name)
    }

    render() {
        return(
            <div className="container">
                <button onClick={this.handleClick} name="register" type="submit">Register for an Account</button>
                <br/>
                <button onClick={this.handleClick} name="verify" type="submit">Verify your Account</button>
                <br/>
                <button onClick={this.handleClick} name="login" type="submit">Login</button>
                <br/>
                <button onClick={this.handleClick} name="passwordReset" type="submit">Reset Password</button>
                <br/>
            </div>
        )
    }
}

export default LRVNav