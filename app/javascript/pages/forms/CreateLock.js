import React from 'react';
import { testPasswordStrength, createDataFiles } from '../../utils/appUtils';

import FullScreenLoader from '../components/FullScreenLoader';

class CreateLock extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            passcode: "",
            passcodeConfirm: "",
            statusVisible: false,
            buttonClicked: false
        }

        this.handleClick = this.handleClick.bind(this)
        this.updatePasscode = this.updatePasscode.bind(this)
        this.updateConfirmPass = this.updateConfirmPass.bind(this)
        this.handleStatus = this.handleStatus.bind(this)
    }

    updatePasscode(evt){
        this.setState({
            passcode: evt.target.value
        })
    }

    updateConfirmPass(evt){
        this.setState({
            passcodeConfirm: evt.target.value
        })
    }

    handleStatus(message){
        document.getElementsByClassName("status")[0].innerHTML = message;

        this.setState({
            statusVisible: true
        })
        setTimeout(()=>{
            this.setState({
                statusVisible: false
            })
        }, 5000)
    }

    handleClick(){
        this.setState({
            buttonClicked: true
        })

        if(this.state.passcode === this.state.passcodeConfirm){
            if(testPasswordStrength(this.state.passcode)){
                createDataFiles(this.state.passcode);
                this.setState({
                    buttonClicked: false
                })
                this.props.changeView("lock")
            }else{
                this.setState({
                    buttonClicked: false
                })
                this.handleStatus("Password must contain at least 1 upper case letter, 1 lowercase letter, 1 digit, 1 special character, and minimum 10 in length");
            }
        }else{
            this.setState({
                buttonClicked: false
            })
            this.handleStatus("Passcodes do not match")
        }
    }

    render(){
        return(
            <div className="container">
                <h1 className="formTitle">Please Create the Local Passcode</h1>
                <hr/>
                <input type="password" placeholder="Passcode" onChange={this.updatePasscode} required/>
                <br/>
                <input type="password" placeholder="Confirm Passcode" onChange={this.updateConfirmPass} required/>
                <br/>
                <button onClick={this.handleClick} disabled={this.state.buttonClicked} type="submit">Login</button>

                <p className={this.state.statusVisible ? 'fadeInStatus status' : 'fadeOutStatus status'}></p>

                <FullScreenLoader visible={this.state.buttonClicked}/>
            </div>
        )
    }
}

export default CreateLock