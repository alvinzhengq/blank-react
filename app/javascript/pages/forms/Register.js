import React from 'react';
import { validateRegisterFormInput, generateSalt, keyStretch } from '../../utils/appUtils';
import axios from 'axios'
import { serverURL, apiPubKey, apiSecret, axiosHeaders } from '../../configs';

import FullScreenLoader from '../components/FullScreenLoader';
import BackButton from '../components/BackButton';

class Register extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            password: "",
            confirmPassword: "",
            username: "",
            email: "",
            statusVisible: ""
        }

        this.updateInputs = this.updateInputs.bind(this);
        this.handleStatus = this.handleStatus.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    updateInputs(evt){
        this.setState({
            [evt.target.name]: evt.target.value
        })
    }

    handleStatus(message){
        document.getElementsByClassName("status")[0].innerHTML = message;

        this.setState({
            statusVisible: true
        })
        setTimeout(()=>{
            this.setState({
                statusVisible: false
            })
        }, 5000)
    }

    handleClick(){
        this.setState({
            buttonClicked: true
        })

        let validation = validateRegisterFormInput(this.state.email, this.state.password, this.state.confirmPassword, this.state.username)
        if(validation === "VALID"){
            var salt = generateSalt();
            var finalPassword = keyStretch(this.state.password + salt);

            axios.post(serverURL+"api/register", apiPubKey.encrypt(JSON.stringify({
                username: this.state.username,
                email: this.state.email,
                password: finalPassword,
                salt: salt,
                apiSecret: apiSecret
            }), 'hex'), {headers: axiosHeaders})
            .then((res) => {
                this.setState({
                    buttonClicked: false
                })

                if(res.data === "OK"){
                    this.props.changeView("verify")
                    this.props.setUsername(this.state.username)
                }else if(res.data === "BAD_USERNAME"){
                    this.handleStatus("Username unavailable ")
                }else if(res.data === "BAD_EMAIL"){
                    this.handleStatus("Email already in use")
                }
            })
            .catch((err) => {
                this.setState({
                    buttonClicked: false
                })
                this.handleStatus(err)
            })
        }else{
            this.setState({
                buttonClicked: false
            })
            this.handleStatus(validation)
        }
    }

    render() {
        return(
            <div className="container">
                <h1 className="formTitle">Register for an Account</h1>
                <hr/>
                <input type="text" placeholder="Username" name="username" onChange={this.updateInputs} required/>
                <br/>
                <input type="text" placeholder="Email" name="email" onChange={this.updateInputs} required/>
                <br/>
                <input type="password" placeholder="Password" name="password" onChange={this.updateInputs} required/>
                <br/>
                <input type="password" placeholder="Confirm Password" name="confirmPassword" onChange={this.updateInputs} required/>
                <br/>
                <button onClick={this.handleClick} disabled={this.state.buttonClicked} type="submit">Register</button>

                <p style={{marginTop: "3vh"}}>Your email will only be used for verification and password resets</p>
                <p className={this.state.statusVisible ? 'fadeInStatus status' : 'fadeOutStatus status'}></p>

                <FullScreenLoader visible={this.state.buttonClicked}/>
                <BackButton changeView={() => this.props.changeView("lrvNav")}/>
            </div>
        )
    }
}

export default Register