import React from 'react';
import axios from 'axios'
import fs from 'fs';
import { serverURL, dataDir, apiSecret, apiPubKey, axiosHeaders } from '../../configs';
import {machineIdSync} from 'node-machine-id';

import FullScreenLoader from '../components/FullScreenLoader';
import BackButton from '../components/BackButton';

class VerifyAcc extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            username: "",
            verificationCode: "",
            buttonClicked: "",
            statusVisible: ""
        }

        this.updateInputs = this.updateInputs.bind(this)
        this.handleStatus = this.handleStatus.bind(this)
        this.handleClick = this.handleClick.bind(this)
    }

    handleStatus(message){
        document.getElementsByClassName("status")[0].innerHTML = message;

        this.setState({
            statusVisible: true
        })
        setTimeout(()=>{
            this.setState({
                statusVisible: false
            })
        }, 5000)
    }

    updateInputs(evt) {
        this.setState({
            [evt.target.name]: evt.target.value
        })
    }

    handleClick() {
        this.setState({
            buttonClicked: true
        })
        if(this.state.username != "" && this.state.verificationCode != ""){
            var machineID = machineIdSync();
            axios.post(serverURL+"api/verify", apiPubKey.encrypt(JSON.stringify({
                username: this.state.username,
                code: this.state.verificationCode,
                machineID: machineID,
                apiSecret: apiSecret
            }), "hex"), {headers: axiosHeaders})
            .then((res) => {
                fs.mkdirSync(dataDir + "/" + this.state.username);

                if(res.data === "EXPIRE"){
                    this.setState({
                        buttonClicked: false
                    })
                    this.handleStatus("Code expired or user does not exist")
                }else if(res.data === "WRONG"){
                    this.setState({
                        buttonClicked: false
                    })
                    this.handleStatus("Code is incorrect")
                }else if(res.data === "Server side error"){
                    this.setState({
                        buttonClicked: false
                    })
                    this.handleStatus("Server side error")
                }else{
                    this.setState({
                        buttonClicked: false
                    })
                    this.props.changeView("login")
                }
            })
            .catch((err) => {
                this.setState({
                    buttonClicked: false
                })
                this.handleStatus(err)
            })

        }else{
            this.setState({
                buttonClicked: false
            })
            this.handleStatus("One or more forms are empty")
        }
    }

    render() {
        return(
            <div className="container">
                <h1 className="formTitle">Verify your Account</h1>
                <hr/>
                <input type="text" placeholder="Username" name="username" onChange={this.updateInputs} required/>
                <br/>
                <input type="text" placeholder="Verification Code" name="verificationCode" onChange={this.updateInputs} required/>
                <br/>
                <button onClick={this.handleClick} disabled={this.state.buttonClicked} type="submit">Submit</button>
                <p>Verifying an account on a device will mark that device as the primary device</p>

                <p className={this.state.statusVisible ? 'fadeInStatus status' : 'fadeOutStatus status'}></p>

                <FullScreenLoader visible={this.state.buttonClicked}/>
                <BackButton changeView={() => this.props.changeView("lrvNav")}/>
            </div>
        )
    }
}

export default VerifyAcc