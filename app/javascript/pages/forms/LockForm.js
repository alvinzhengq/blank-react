import React from 'react';
import { lockCheck, loginCheck } from '../../utils/appUtils';

import FullScreenLoader from '../components/FullScreenLoader';

class LockForm extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            passcode: "",
            statusVisible: false,
            buttonClicked: false
        }

        this.handleClick = this.handleClick.bind(this)
        this.updatePasscode = this.updatePasscode.bind(this)
        this.handleStatus = this.handleStatus.bind(this)
    }

    updatePasscode(evt){
        this.setState({
            passcode: evt.target.value
        })
    }

    handleStatus(message){
        document.getElementsByClassName("status")[0].innerHTML = message;

        this.setState({
            statusVisible: true
        })
        setTimeout(()=>{
            this.setState({
                statusVisible: false
            })
        }, 5000)
    }

    handleClick() {
        this.setState({
            buttonClicked: true
        })

        if(lockCheck(this.state.passcode)){
            this.props.setPasscode(this.state.passcode)

            loginCheck(this.state.passcode, (res) => {
                this.setState({
                    buttonClicked: false
                })

                if(res === "REGISTER"){
                    this.props.changeView("lrvNav")
                }else{
                    if(res.refreshToken != ""){
                        this.props.setRefreshToken(res.refreshToken);
                        this.props.setUsername(res.username);
                        this.props.changeView("app")
                    }else{
                        this.props.changeView("login")
                    }
                }
            })

        }else{
            this.handleStatus("Passcode is incorrect")
            this.setState({
                buttonClicked: false
            })
        }
    }

    render(){
        return(
            <div className="container">
                <h1 className="formTitle">Please Enter the Local Passcode</h1>
                <hr/>
                <input type="password" placeholder="Passcode" onChange={this.updatePasscode} required/>
                <br/>
                <button onClick={this.handleClick} disabled={this.state.buttonClicked} type="submit">Login</button>

                <p className={this.state.statusVisible ? 'fadeInStatus status' : 'fadeOutStatus status'}></p>

                <FullScreenLoader visible={this.state.buttonClicked}/>
            </div>
        )
    }
}

export default LockForm