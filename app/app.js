const { app, BrowserWindow } = require("electron");
const path = require("path");

if (app.commandLine.getSwitchValue("remote-debugging-port")) {
  app.quit();
}

function createWindow() {
  const mainWindow = new BrowserWindow({
    width: 1100,
    height: 650,
    webPreferences: {
      nodeIntegration: true,
      //devTools: false
    },
  });

  mainWindow.webContents.session.webRequest.onBeforeSendHeaders(
    (details, callback) => {
      details.requestHeaders["User-Agent"] =
        "Mozilla/5.0 (Windows NT 10.0; rv:68.0) Gecko/20100101 Firefox/68.0";
      callback({ cancel: false, requestHeaders: details.requestHeaders });
    }
  );

  // mainWindow.webContents.session.setProxy({proxyRules: "socks5://localhost:9050"})
  // mainWindow.removeMenu();
  // mainWindow.webContents.on("devtools-opened", () => {mainWindow.webContents.closeDevTools(); });

  mainWindow.loadFile(path.join(__dirname + "/views/index.html"));
}

app.whenReady().then(() => {
  createWindow();

  app.on("activate", function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

app.on("window-all-closed", function () {
  if (process.platform !== "darwin") app.quit();
});
