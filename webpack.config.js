const path = require('path');
const nodeExternals = require('webpack-node-externals');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const WebpackObfuscator = require('webpack-obfuscator');

module.exports = {
    mode: "production",
    target: 'node',
    externals: [nodeExternals()],

    entry: './app/javascript/index.js',
    output: {
        filename: "index-bundle.js",
        path: path.join(__dirname + "/app/dist/")
    },

    module: {
        rules: [
            {
                test: /\.(js|ts)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },

            {
                test: /\.s?css$/,
                use: ['style-loader', 'css-loader']
            },

            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'fonts/',
                    publicPath: url => '../assets/fonts/' + url
                }
            },
        ],
    },

    /**
    optimization: {
        minimizer: [new UglifyJsPlugin()],
    },

    plugins: [
        new WebpackObfuscator ({
            identifierNamesGenerator: 'mangled',
            target: "node",
            stringArray: true,
            stringArrayEncoding: "rc4",
            splitStrings: true,
            shuffleStringArray: true,
            rotateStringArray: true,
            disableConsoleOutput: true,
        }, [])
    ],
    */
}