# Blank Chat Client

A decentralized e2e encrypted native chat application routed over the Tor network. Built using websocket technology, JWT authentication, and Express.js + MongoDB backend.
